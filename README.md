# Elasticsearch and Kibana
This repository contains docker files that describe Elasticsearch and Kibana deployment. All dockers are composed in docker-compose.yml that consits of 
these services

* elasticsearch
* kibana
* nginx

#### Elasticsearch
The Elasticsearch service is described in the docker file **elasticsearch/Docker** and use **elasticsearch/config/elasticsearch.yml**. The Elasticsearch data are stored in **/mnt/data/elasticsearch** that is folder of mounted storage to prevent lost of data. 
Elasticsearch service is running on port 9200.

#### Kibana
The Kibana service is described in the docker file **kibana/Docker** and use **kibana/config/kibana.yml**. Kibana is running on port 5601.

#### Nginx
The Nginx service is used to provide basic authentication to Elasticsearch and Kibana. This service use config **nginx/nginx.conf**.
It translates open ports 9300 to http://elasticsearch:9200 and 5602 to http://kibana:5601.  

The username and password that are used in basic authentication is stored in **nginx/htpasswd**. To create a new username and password use 
one of the public available tutorials (e.g. https://www.digitalocean.com/community/tutorials/how-to-set-up-password-authentication-with-nginx-on-ubuntu-14-04)

## Deployment
Deployment of Elasticsearch and Kibana with Nginx authentication consists of these steps:

* clone this repo to machine you want to deploy (you must to ensure appropriate ports are open)
* mount disk to /mnt/data where Elasticsearch will store its data
* run docker-compose up -d (-d means detached - without providing logs on output)  

Current deployment is running on EC2 machine **elastic-kibana** (Ireland). This machine is accessible with key **jozef-biotron-ec2** (was sent to Slavo and Marek)

# Troubleshooting
In the case of current ES+Kibana docker compose instance is not running or something not correct is happening, the best way is  

* connect to EC2 machine **elastic-kibana** (Ireland) (according to prev section)
* check state of machine (disk, ram etc)
* do to folder **/home/ubuntu/biotron-elastic-kibana**
* run commands `sudo docker-compose down` and `sudo docker-compose up -d`
